#!/usr/bin/env python
# encoding: utf-8
from pprint import pprint
import pytest
import os
import sys
import time
import shlex

from pocker import Docker
from pocker.pocker import PockerResp, reject_opts
from pocker.pocker import castval, castkey, getopts
from pocker.exceptions import (PockerException, PEnvVarsError, PDockerLauncherError,
                               PNotYetImplemented, PUnsupportedOption, PExitNonZero,
                               PVersionError, PRejectedOption)
from pocker.utils import required_version, wait_net_service, Ver, qdict


class TestRequiredVersion(object):
    def test_without_space(self):
        try:
            required_version('1.5', ['>1.4'])
        except PockerException as err:
            assert 'Wrong version specification' in err.message

    def test_version_specs(self):
        required_version('1.5', ['> 1.4.1', '<= 1.5'])
        required_version('1.5.1', ['== 1.5.1'])
        with pytest.raises(PVersionError):
            required_version('1.5', ['> 1.6', '< 1.7'])


class TestCommon(object):
    def test_no_envvars(self, mocker):
        mock = mocker.Mock()
        mock.result = {"Client": {"Version": "1.8.0"}}
        mocker.patch.object(Docker, 'version', return_value=mock)
        assert Docker().envvars is None

    def test_envvars_must_be_strings(self):
        with pytest.raises(PEnvVarsError):
            Docker(_envvars={'non_string': 1})

    def test_check_docker_installed(self, mocker):
        def os_error():
            raise OSError(os.errno.ENOENT, os.strerror(os.errno.ENOENT))
        mocker.patch.object(Docker, 'version', side_effect=os_error)
        with pytest.raises(PDockerLauncherError):
            Docker()

    def test_castval(self):
        assert castval(True) == 'true'
        assert castval(False) == 'false'
        assert castval(1) == 1

    def test_castkey(self):
        assert castkey('key') == 'key'
        assert castkey('complex_key') == 'complex-key'

    def test_getopts(self):
        assert getopts(**{'_internal': True}) == []
        assert getopts(**{'none_val': None}) == []
        assert getopts(**{'callable': lambda: 'lambda'}) == ['--callable=lambda']
        assert getopts(**{'string': 'string_value'}) == ['--string=string_value']
        assert getopts(**{'seq': [1,2]}) == ['--seq=1','--seq=2']
        assert getopts(**{'t': True}) == ['-t=true']

    def test_pocker_resp(self):
        resp = PockerResp('stdout\nraw\n', 'stderr\nraw\n', 1, True,
                          suppress_excp=True)
        assert str(resp) is not None
        assert resp.stdout_raw == 'stdout\nraw\n'
        assert resp.stdout_lines == ['stdout', 'raw', '']
        assert resp.stderr_raw == 'stderr\nraw\n'
        assert resp.stderr_lines == ['stderr', 'raw', '']
        assert resp.exit_code == 1
        assert resp.result is True

        with pytest.raises(PExitNonZero):
            PockerResp('stdout\nraw\n', 'stderr\nraw\n', 1, True)

        try:
            resp = PockerResp('stdout\nraw\n', 'stderr\nraw\n', 1, True)
        except PExitNonZero as err:
            assert str(err) is not None
            assert unicode(err) is not None
            resp = err.resp
            assert resp.stdout_raw == 'stdout\nraw\n'
            assert resp.stdout_lines == ['stdout', 'raw', '']
            assert resp.stderr_raw == 'stderr\nraw\n'
            assert resp.stderr_lines == ['stderr', 'raw', '']
            assert resp.exit_code == 1
            assert resp.result is True

    def test_qdict(self):
        q = qdict({'key': 'value'})
        assert isinstance(q, dict)
        assert q['key'] == 'value'
        assert q.key == 'value'

        q.key = 'new_value'
        assert q['key'] == 'new_value'
        assert q.key == 'new_value'

        q.new_key = True
        assert q['new_key'] == True
        assert q.new_key == True

        q.update({'key': 'update', 'from_update': True})
        assert q.key == 'update'
        assert q.from_update == True

        q.setdefault('default', True)
        assert q.default == True

        with pytest.raises(AttributeError):
            q.no_key

        try:
            q.no_key
        except AttributeError as err:
            assert 'no_key' in err.message

    def test_reject_opts(self):
        opts = {'to_reject': True, 'valid_opt': True}
        try:
            reject_opts(opts, 'to_reject stub some_opt')
        except PRejectedOption as err:
            assert 'to_reject' in err.message

        opts = {'option': True, 'valid_opt': True}
        reject_opts(opts, 'to_reject stub some_opt')




@pytest.yield_fixture(scope="session")
def host_docker(request):
    #create pocker client connected to host docker to manage docker-in-docker instances
    yield Docker()

def idfn(fixture_docker):
    version, launcher, cmd, port = fixture_docker
    return version

@pytest.yield_fixture(scope="module", ids=idfn, params=[
    ("1.5.0", "/home/vagrant/.dkenv/docker-1.5.0", "docker --daemon", "4000"),
    ("1.6.2", "/home/vagrant/.dkenv/docker-1.6.2", "docker --daemon", "4001"),
    ("1.7.1", "/home/vagrant/.dkenv/docker-1.7.1", "docker --daemon", "4002"),
    ("1.8.3", "/home/vagrant/.dkenv/docker-1.8.3", "docker daemon", "4003"),
    ("1.9.1", "/home/vagrant/.dkenv/docker-1.9.1", "docker daemon", "4004"),
    ("1.10.3", "/home/vagrant/.dkenv/docker-1.10.3", "docker daemon", "4005"),
    ("1.11.2", "/home/vagrant/.dkenv/docker-1.11.2", "docker daemon", "4006"),
    ("1.12.2", "/home/vagrant/.dkenv/docker-1.12.2", "docker daemon", "4006"),
])
def docker(request, host_docker):
    version, launcher, cmd, port = request.param
    img = "test-docker:"+version
    cnt_name = img.replace(':', '-')

    try:
        host_docker.rm(cnt_name)
    except PExitNonZero:
        pass
    #using host docker to run docker-in-docker daemon as target for tests
    resp = host_docker.run(
        img, *shlex.split(cmd+" --host=tcp://0.0.0.0:2375 -s aufs"),
        d=True, privileged=True, p="127.0.0.1:"+port+":2375", volume="/var/lib/docker",
        name=cnt_name
    )
    docker_in_docker_id = resp.result["Id"]
    assert wait_net_service("127.0.0.1", int(port), 5)
    time.sleep(0.5)
    #create pocker client connected to started docker-in-docker daemon
    #with appropriate version of docker client
    docker = Docker(_launcher=launcher, host="127.0.0.1:"+port)

    #create test image(fixture) inside docker-in-docker
    context_file = "/home/vagrant/source/dev/test-img-context.tar.gz"
    with open(context_file, mode='rb') as file: # b is important -> binary
        context = file.read()
    docker.build('-', tag='test-img', _input=context)

    yield docker

    host_docker.rm(docker_in_docker_id, force=True, volumes=True)

def test_version(docker):
    assert docker.version().result['Server']['Version'] is not None

def test_build_images_rmi(docker):
    img_id = docker.build('-', tag='test_build',
                          _input=('FROM test-img\n'
                                  'RUN touch test')).result['Id']
    assert img_id
    assert 'test_build' in docker.images().stdout_raw
    docker.rmi('test_build')
    assert 'test_build' not in docker.images().stdout_raw

def test_create_inspect_start_rm(docker):
    docker.create('test-img', *shlex.split('sleep 10'),
                  name='test_create')
    assert docker.inspect('test_create').result[0]["State"]["Running"] is False
    docker.start('test_create')
    assert docker.inspect('test_create').result[0]["State"]["Running"] is True
    docker.rm('test_create', force=True)

def test_run_pause_unpause(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_pause', d=True)
    assert docker.inspect('test_pause').result[0]["State"]["Running"] is True
    assert docker.inspect('test_pause').result[0]["State"]["Paused"] is False
    docker.pause('test_pause')
    assert docker.inspect('test_pause').result[0]["State"]["Paused"] is True
    docker.unpause('test_pause')
    assert docker.inspect('test_pause').result[0]["State"]["Paused"] is False
    docker.rm('test_pause', force=True)

def test_stop(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_stop', d=True)
    assert docker.inspect('test_stop').result[0]["State"]["Running"] is True
    docker.stop('test_stop', time=1)
    assert docker.inspect('test_stop').result[0]["State"]["Running"] is False
    docker.rm('test_stop')

def test_diff_exec(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_diff', d=True)
    assert docker.inspect('test_diff').result[0]["State"]["Running"] is True
    assert docker.exec_('test_diff', *shlex.split('touch test_diff'))
    if Ver(docker.current_version) >= Ver('1.11.1'):
        #FIXME: some docker bug? diff see changes only after timeout
        time.sleep(0.1)
    diff = docker.diff('test_diff')
    assert len(diff.result) == 1
    assert diff.result[0]['Path'] == '/test_diff'
    assert diff.result[0]['Kind'] == 1
    docker.rm('test_diff', force=True)

def test_history(docker):
    assert len(docker.history('test-img').result) == 1

def test_images(docker):
    img_id = docker.build('-', tag='test_images',
                          _input=('FROM test-img\n'
                                  'RUN touch test')).result['Id']
    assert img_id
    images = docker.images()
    assert len(images.result) == 2
    assert ('test_images:latest' in images.result[0]['RepoTags']) or ('test_images:latest' in images.result[1]['RepoTags'])
    docker.rmi('test_images')

def test_info(docker):
    info = docker.info()
    assert info.result['CPUs'] == '1'
    assert info.result['Storage Driver'] == 'aufs'

def test_kill(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_kill', d=True)
    docker.kill('test_kill')
    docker.rm('test_kill')

def test_logs(docker):
    docker.run('test-img', *shlex.split('echo "test_logs"'),
               name='test_logs', d=True)
    logs = docker.logs('test_logs')
    assert "test_logs" in logs.stdout_raw
    docker.rm('test_logs')

def test_port(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_port', d=True, expose=[2000, 2001],
               publish=["0.0.0.0:3000:2000/udp",
                        "0.0.0.0:3001:2001/tcp"])
    port = docker.port('test_port')
    assert port.result['2000/udp'] == {'HostIp': '0.0.0.0', 'HostPort': '3000'}
    assert port.result['2001/tcp'] == {'HostIp': '0.0.0.0', 'HostPort': '3001'}
    port = docker.port('test_port', '2001')
    assert port.result == {'HostIp': '0.0.0.0', 'HostPort': '3001'}
    docker.rm('test_port', force=True)

def test_ps(docker):
    cnt = docker.run('test-img', *shlex.split('sleep 10'),
                      name='test_ps', d=True)
    ps = docker.ps()
    assert len(ps.result) == 1
    assert ps.result[0]['Name'] == 'test_ps'
    if docker.current_version in ('1.5.0','1.6.2'):
        assert ps.result[0]['Image'] == 'test-img:latest'
    else:
        assert ps.result[0]['Image'] == 'test-img'
    assert ps.result[0]['Id'] == cnt.result['Id']
    if docker.current_version not in ('1.5.0'):
        #must be fixed in docker 1.12.3
        #https://github.com/docker/docker/issues/27047
        #https://github.com/docker/docker/pull/27333
        ps = docker.ps(filter='label=pocker._beacon="test_cnt"')
        assert len(ps.result) == 0
    docker.rm('test_ps', force=True)

def test_pull(docker):
    #TODO: use cache for images
    #https://github.com/jpetazzo/squid-in-a-can
    if Ver(docker.current_version) == Ver('1.5.0'):
        pytest.skip("Docker 1.5.0 is no more supported by publick docker registry .")
    docker.pull('tianon/true')
    assert 'tianon/true:latest' in [img['RepoTags'][0] for img in docker.images().result]
    docker.rmi('tianon/true')
    assert 'tianon/true:latest' not in [img['RepoTags'][0] for img in docker.images().result]

def test_rename(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_rename', d=True)
    docker.rename('test_rename', 'test_renamed')
    docker.rm('test_renamed', force=True)

def test_restart(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_restart', d=True)
    assert docker.inspect('test_restart').result[0]["State"]["Running"] is True
    docker.restart('test_restart', time=1)
    assert docker.inspect('test_restart').result[0]["State"]["Running"] is True
    docker.rm('test_restart', force=True)

def test_tag(docker):
    images = docker.images()
    assert len(images.result) == 1
    assert 'test-img:added_tag' not in images.result[0]['RepoTags']
    assert docker.tag('test-img:latest', 'test-img:added_tag')
    images = docker.images()
    assert len(images.result) == 1
    assert 'test-img:added_tag' in images.result[0]['RepoTags']
    docker.rmi('test-img:added_tag')
    images = docker.images()
    assert len(images.result) == 1
    assert 'test-img:added_tag' not in images.result[0]['RepoTags']

def test_stats(docker):
    #TODO: add more tests, after parser for stats added
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_stats', d=True)
    if Ver(docker.current_version) < Ver('1.7.1'):
        with pytest.raises(PNotYetImplemented):
            stats = docker.stats('test_stats')
        return
    else:
        stats = docker.stats('test_stats')

    # 3 = headers + stats data for container + new line
    assert len(stats.stdout_lines) == 3
    assert 'test_stats' in stats.stdout_raw
    docker.rm('test_stats', force=True)

def test_top(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
                      name='test_top', d=True)
    docker.exec_('test_top', *shlex.split('sleep 99'),
                        detach=True)
    top = docker.top('test_top')
    if Ver(docker.current_version) < Ver('1.6.0'):
        #3 = sleep 10 + sleep 99 + nsenter-exec proc
        assert len(top.result['Processes']) == 3
        procs = top.result['Processes']
        assert 'sleep 10' in (procs[0][-1],procs[1][-1],procs[2][-1])
        assert 'sleep 99' in (procs[0][-1],procs[1][-1],procs[2][-1])
    else:
        assert len(top.result['Processes']) == 2
        assert top.result['Processes'][0][-1] in ('sleep 10', 'sleep 99')
        assert top.result['Processes'][1][-1] in ('sleep 10', 'sleep 99')

    docker.rm('test_top', force=True)

def test_wait(docker):
    docker.run('test-img', *shlex.split('sleep .5'),
                      name='test_wait', d=True)
    wait = docker.wait('test_wait')
    assert wait.result['StatusCode'] == 0
    docker.rm('test_wait', force=True)

    docker.run('test-img', *shlex.split("sh -c 'sleep 1; exit 1'"),
                      name='test_wait2', d=True)
    wait = docker.wait('test_wait2')
    assert wait.result['StatusCode'] == 1
    docker.rm('test_wait2', force=True)

def test_labels(docker):
    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_labels0', d=True)
    if Ver(docker.current_version) < Ver('1.6.0'):
        with pytest.raises(PUnsupportedOption):
            docker.run('test-img', *shlex.split('sleep 10'),
                       name='test_labels1', d=True,
                       label=['label1=value1', 'label2=value2'])
        return

    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_labels1', d=True,
               label=['label1=value1', 'label2=value2'])

    inspect = docker.inspect('test_labels1')
    assert inspect.result[0]['Config']['Labels']['label1'] == 'value1'
    assert inspect.result[0]['Config']['Labels']['label2'] == 'value2'

    docker.run('test-img', *shlex.split('sleep 10'),
               name='test_labels2', d=True,
               label='label1=value3')

    ps = docker.ps(filter="label=label1")
    assert len(ps.result) == 2

    ps = docker.ps(filter="label=label1=value3")
    assert len(ps.result) == 1
    assert ps.result[0]['Name'] == 'test_labels2'

def test_suppress_excp(docker):
    with pytest.raises(PExitNonZero):
        docker.stop('fail')

    stop = docker.stop('fail', _suppress_excp=True)
    assert stop.exit_code != 0

    suppressed_docker = Docker(docker.launcher, docker.docker_opts,
                               _suppress_excp=True)
    stop = suppressed_docker.stop('fail')
    assert stop.exit_code != 0

def test_network_create_rm_ls_inspect(docker):
    #TODO: tests for overlay network?
    if Ver(docker.current_version) < Ver('1.9.0'):
        pytest.skip("Not supported in this version.")
    net1_id = docker.network_create('net1').result['Id']
    net2_id = docker.network_create('net2').result['Id']
    net_ls = docker.network_ls().result
    assert len(net_ls) == 5
    assert net_ls[0]['Id'] is not None
    assert net_ls[0]['Name'] is not None
    assert net_ls[0]['Driver'] is not None
    assert filter(lambda net: net['Name'] == 'net1', net_ls)[0]['Id'] == net1_id
    assert filter(lambda net: net['Name'] == 'net2', net_ls)[0]['Id'] == net2_id

    inspect = docker.network_inspect('net1').result[0]
    assert inspect['Name'] == 'net1'
    assert inspect['Id'] == net1_id
    assert inspect['Driver'] == 'bridge'

    docker.network_rm('net1')
    assert not 'net1' in [net['Name'] for net in docker.network_ls().result]
    assert 'net2' in [net['Name'] for net in docker.network_ls().result]
    docker.network_rm('net2')
    assert not 'net1' in [net['Name'] for net in docker.network_ls().result]
    assert not 'net2' in [net['Name'] for net in docker.network_ls().result]

def test_network_connect_disconnect(docker):
    #TODO: tests for overlay network?
    if Ver(docker.current_version) < Ver('1.9.0'):
        pytest.skip("Not supported in this version.")

    net1_id = docker.network_create('net1').result['Id']
    assert net1_id

    cnt1_id = docker.run('test-img', *shlex.split('sleep 10'),
               name='test_net1', d=True, net='net1').result['Id']
    cnt2_id = docker.run('test-img', *shlex.split('sleep 10'),
               name='test_net2', d=True).result['Id']
    inspect = docker.network_inspect('net1').result[0]
    assert len(inspect['Containers']) == 1
    assert inspect['Containers'][cnt1_id]

    docker.network_connect('net1', cnt2_id)
    inspect = docker.network_inspect('net1').result[0]
    assert len(inspect['Containers']) == 2
    assert inspect['Containers'][cnt1_id]
    assert inspect['Containers'][cnt2_id]

    docker.network_disconnect('net1', cnt1_id)
    inspect = docker.network_inspect('net1').result[0]
    assert len(inspect['Containers']) == 1
    assert inspect['Containers'][cnt2_id]

    docker.network_disconnect('net1', cnt2_id)
    inspect = docker.network_inspect('net1').result[0]
    assert len(inspect['Containers']) == 0

    docker.rm('test_net1', force=True)
    docker.rm('test_net2', force=True)
    docker.network_rm('net1')

def test_volume_create_rm_ls_inspect(docker):
    if Ver(docker.current_version) < Ver('1.9.0'):
        pytest.skip("Not supported in this version.")

    vol1 = docker.volume_create(name='vol1').result['Name']
    vol2 = docker.volume_create(name='vol2').result['Name']
    assert all([vol1, vol2])

    vol_ls = docker.volume_ls().result
    assert len(vol_ls) == 2
    assert vol_ls[0]['Name'] is not None
    assert vol_ls[0]['Driver'] is not None
    assert filter(lambda vol: vol['Name'] == 'vol1', vol_ls)[0]['Name'] == vol1
    assert filter(lambda vol: vol['Name'] == 'vol2', vol_ls)[0]['Name'] == vol2

    inspect = docker.volume_inspect('vol1').result[0]
    assert inspect['Name'] == vol1
    assert inspect['Driver'] == 'local'
    assert inspect['Mountpoint']

    docker.volume_rm('vol1')
    assert not 'vol1' in [vol['Name'] for vol in docker.volume_ls().result]
    assert 'vol2' in [vol['Name'] for vol in docker.volume_ls().result]
    docker.volume_rm('vol2')
    assert not 'vol1' in [vol['Name'] for vol in docker.volume_ls().result]
    assert not 'vol2' in [vol['Name'] for vol in docker.volume_ls().result]

def test_not_implemented(host_docker):
    assert str(PNotYetImplemented()) is not None

    docker = host_docker
    with pytest.raises(PNotYetImplemented):
        docker.attach()
    with pytest.raises(PNotYetImplemented):
        docker.commit()
    with pytest.raises(PNotYetImplemented):
        docker.cp()
    with pytest.raises(PNotYetImplemented):
        docker.events()
    with pytest.raises(PNotYetImplemented):
        docker.export()
    with pytest.raises(PNotYetImplemented):
        docker.import_()
    with pytest.raises(PNotYetImplemented):
        docker.load()
    with pytest.raises(PNotYetImplemented):
        docker.login()
    with pytest.raises(PNotYetImplemented):
        docker.logout()
    with pytest.raises(PNotYetImplemented):
        docker.push()
    with pytest.raises(PNotYetImplemented):
        docker.save()
    with pytest.raises(PNotYetImplemented):
        docker.search()

#TODO:
#daemon
