#!/usr/bin/env bash

echo "Build images of docker daemons for tests ..."
sudo docker build -t test-docker:1.5.0 - < ~/source/dev/Dockerfile-1.5.0
sudo docker build -t test-docker:1.6.2 - < ~/source/dev/Dockerfile-1.6.2
sudo docker build -t test-docker:1.7.1 - < ~/source/dev/Dockerfile-1.7.1
sudo docker build -t test-docker:1.8.3 - < ~/source/dev/Dockerfile-1.8.3
sudo docker build -t test-docker:1.9.1 - < ~/source/dev/Dockerfile-1.9.1
sudo docker build -t test-docker:1.10.3 - < ~/source/dev/Dockerfile-1.10.3
sudo docker build -t test-docker:1.11.2 - < ~/source/dev/Dockerfile-1.11.2
sudo docker build -t test-docker:1.12.2 - < ~/source/dev/Dockerfile-1.12.2
