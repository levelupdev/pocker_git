#!/usr/bin/env bash
sudo apt-get update

echo "Update kernel for aufs..."
sudo apt-get -y install linux-image-generic-lts-trusty

echo "Installing git..."
sudo apt-get -y install git

echo "Creating go workspace..."
HOME=/home/vagrant
mkdir -p $HOME/go/{src,bin,pkg}
export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$PATH
if ! grep -q "export GOPATH" /etc/bash.bashrc; then
    echo "export GOPATH=$HOME/go" | sudo tee --append /etc/bash.bashrc > /dev/null
    echo "export PATH=$GOPATH/bin:$PATH" | sudo tee --append /etc/bash.bashrc > /dev/null
fi
if ! grep -q "export GOPATH" /etc/profile; then
    echo "export GOPATH=$HOME/go" | sudo tee --append /etc/profile > /dev/null
    echo "export PATH=$GOPATH/bin:$PATH" | sudo tee --append /etc/profile > /dev/null
fi

echo "Installing golang using godeb..."
wget https://godeb.s3.amazonaws.com/godeb-amd64.tar.gz -O godeb.tar.gz
tar -zxvf godeb.tar.gz
mv godeb $GOPATH/bin/
godeb install 1.5.3

echo "Installing dkenv..."
go get github.com/newrelic/dkenv

echo "Installing docker(client to use in testing) versions..."
#--bindir="/tmp/" - to avoid permission denied errors: 
#   Unable to create new docker symlink: symlink /home/vagrant/.dkenv/docker-1.9.1 /usr/local/bin/docker
dkenv --bindir="/tmp/" client 1.10.3
dkenv --bindir="/tmp/" client 1.9.1
dkenv --bindir="/tmp/" client 1.8.3
dkenv --bindir="/tmp/" client 1.7.1
dkenv --bindir="/tmp/" client 1.6.2
dkenv --bindir="/tmp/" client 1.5.0

#can't use dkenv for docker > 1.11 for now
#https://github.com/newrelic/dkenv/issues/15
#docker 1.11.2
set -x \
&& curl -fSL "https://get.docker.com/builds/Linux/x86_64/docker-1.11.2.tgz" -o docker.tgz \
&& echo "8c2e0c35e3cda11706f54b2d46c2521a6e9026a7b13c7d4b8ae1f3a706fc55e1 *docker.tgz" | sha256sum -c - \
&& tar -xzvf docker.tgz docker/docker \
&& mv docker/docker ~/.dkenv/docker-1.11.2 \
&& rmdir docker \
&& rm docker.tgz \
&& ~/.dkenv/docker-1.11.2 -v

#docker 1.12.2
set -x \
&& curl -fSL "https://get.docker.com/builds/Linux/x86_64/docker-1.12.2.tgz" -o docker.tgz \
&& echo "cb30ad9864f37512c50db725c14a22c3f55028949e4f1e4e585a6b3c624c4b0e *docker.tgz" | sha256sum -c - \
&& tar -xzvf docker.tgz docker/docker \
&& mv docker/docker ~/.dkenv/docker-1.12.2 \
&& rmdir docker \
&& rm docker.tgz \
&& ~/.dkenv/docker-1.12.2 -v

echo "Installing host docker(to start docker-in-docker instances)..."
sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
echo "deb https://apt.dockerproject.org/repo ubuntu-trusty main" | sudo tee /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get -y install docker-engine=1.9.1-0~trusty
sudo usermod -aG docker vagrant

echo "Force docker to use aufs..."
echo 'DOCKER_OPTS="-s aufs"' | sudo tee --append /etc/default/docker > /dev/null
sudo service docker restart

echo "Install additional pythons(2.6, 3.5) for tests ..."
sudo add-apt-repository -y ppa:fkrull/deadsnakes
sudo apt-get update
sudo apt-get -y install python2.6 python2.6-dev
sudo apt-get -y install python3.5 python3.5-dev

echo "Getting pip..."
wget https://bootstrap.pypa.io/get-pip.py -O get-pip.py
sudo python get-pip.py

echo "Installing development deps..."
sudo pip install -r ~/source/dev/requirements.txt
sudo chown -hR vagrant:vagrant ~/.config; #fix for pudb
cd ~/source
sudo python setup.py develop
